import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

//Rutas
import { AppRoutingModule } from './app-routing.module';
import { APP_ROUTING } from './app.routes';

//Servicios
import { ComputosService } from './services/computos.service';
import { PrepService } from './services/prep.service';

//Componentes
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { ComputosComponent } from './components/computos/computos.component';
import { PrepComponent } from './components/prep/prep.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ComputosComponent,
    PrepComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpModule,
    APP_ROUTING
  ],
  providers: [ComputosService, PrepService],
  bootstrap: [AppComponent]
})
export class AppModule { }
