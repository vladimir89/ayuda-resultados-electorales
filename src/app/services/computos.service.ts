import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ComputosService {
  Url = "./assets/data/data.json"

  constructor(private _http:HttpClient) {

   }

   getcomputos(){
     return this._http.get(this.Url)
   }
}
