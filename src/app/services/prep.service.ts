import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class PrepService {
  Url = "./assets/data/prep.json"

  constructor(private _http:HttpClient) {

   }

   getprep(){
     return this._http.get(this.Url)
   }
}
