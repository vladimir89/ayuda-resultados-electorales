import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  goINE() {
    window.open("https://www.ine.mx/", "_blank");
  }

  constructor() { }

  ngOnInit() {
  }

}
