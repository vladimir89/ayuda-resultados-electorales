import { Component, OnInit } from '@angular/core';
import { ComputosService } from '../../services/computos.service';
// import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-computos',
  templateUrl: './computos.component.html',
  styleUrls: ['./computos.component.scss']
})
export class ComputosComponent implements OnInit {

  img_default_c;
  nombre_url_default_c;
  url_default_c;
  nombre_default_c;
  opcionSeleccionado:string='';
  infoComputos:any=[];
  solocomputos:any=[];
  verInfo:any=[];

  constructor(private computos:ComputosService) { }

  ngOnInit() {
    this.getletras();
  }

  getletras(){
    this.computos.getcomputos()
    .subscribe(data=>{
      this.infoComputos=data;

      //Generación de variables de Default
      this.img_default_c = this.infoComputos[0].img;
      this.nombre_url_default_c = this.infoComputos[0].nombreUrl;
      this.url_default_c = this.infoComputos[0].url;
      this.nombre_default_c = this.infoComputos[0].nombre;

    },error=>{
      console.log(error)
    })
  }

  capturar(posicion) {
    this.verInfo = this.infoComputos[posicion];
    console.log(this.verInfo)
  }

}
