import { Component, OnInit } from '@angular/core';
import { PrepService } from '../../services/prep.service';

@Component({
  selector: 'app-prep',
  templateUrl: './prep.component.html',
  styleUrls: ['./prep.component.scss']
})
export class PrepComponent implements OnInit {

  img_default;
  nombre_url_default;
  url_default;
  nombre_default;
  opcionSeleccionado:string='';
  infoPrep:any=[];
  solocomputos:any=[];
  verInfo:any=[];

  constructor(private prep:PrepService) { }

  ngOnInit() {
    this.getletras();
  }

  getletras(){
    this.prep.getprep()
    .subscribe(data=>{
      this.infoPrep=data;

      //Generación de variables de Default
      this.img_default = this.infoPrep[0].img;
      this.nombre_url_default = this.infoPrep[0].nombreUrl;
      this.url_default = this.infoPrep[0].url;
      this.nombre_default = this.infoPrep[0].nombre;


    },error=>{
      console.log(error)
    })
  }

  capturar(posicion) {
    this.verInfo = this.infoPrep[posicion];
    console.log(this.verInfo.id)
    console.log(posicion)
  }



 //  seleccionDefault(variable){
 //
 //
 // }

}
