import { RouterModule, Routes } from '@angular/router';
import { ComputosComponent } from "./components/computos/computos.component";
import { PrepComponent } from "./components/prep/prep.component";



const APP_ROUTES: Routes = [
  { path: 'index/Computos', component: ComputosComponent },
  { path: 'index/Prep', component: PrepComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'index/Prep' }
];

// export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:true});
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
